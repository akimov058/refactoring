﻿function translate_digit(){
	document.getElementById("result").innerHTML = '<p >Результат</p>';

	var i=document.getElementById("input");
	var input = parseInt(i.value);
	var notDigital = /[^0-9]/;
		
	if (!notDigital.test(input)){	
		document.getElementById("result_2").innerHTML = 'Двоичная система счисления';
		document.getElementById("res_2").innerHTML = parseInt(input, 10).toString(2);
		document.getElementById("result_8").innerHTML = 'Восьмиричная система счисления';
		document.getElementById("res_8").innerHTML = parseInt(input, 10).toString(8);
		document.getElementById("result_16").innerHTML = 'Шестнадцатричная система счисления';
		document.getElementById("res_16").innerHTML = parseInt(input, 10).toString(16);
	}	
	else{
		alert("Введенные данные не являются десятичным числом!!!");
	}
}
function translate_bin(){
	document.getElementById("result").innerHTML = '<p >Результат</p>';
	
	var i=document.getElementById("input");
	var input = i.value;
	var notBinary = /[^01]/;
		
	if (!notBinary.test(input)){      		
		document.getElementById("result_8").innerHTML = 'Восьмиричная система счисления';
		document.getElementById("res_8").innerHTML = parseInt(input, 2).toString(8);
		document.getElementById("result_10").innerHTML = 'Десятичная система счисления';
		document.getElementById("res_10").innerHTML = parseInt(input, 2).toString(10);
		document.getElementById("result_16").innerHTML = 'Шестнадцатричная система счисления';
		document.getElementById("res_16").innerHTML = parseInt(input, 2).toString(16);		
	}
	else{
		alert("Введенные данные не являются двоичным кодом!");
		}	
}

function translate_oct(){
	document.getElementById("result").innerHTML = '<p >Результат</p>';
	
	var i=document.getElementById("input");
	var input =  parseInt(i.value);
	var notOct = /[^0-7]/;
		
	if (!notOct.test(input)){
		document.getElementById("result_2").innerHTML = 'Двоичная система счисления';
		document.getElementById("res_2").innerHTML = parseInt(input, 8).toString(2);
		document.getElementById("result_10").innerHTML = 'Десятичная система счисления';
		document.getElementById("res_10").innerHTML = parseInt(input, 8).toString(10);
		document.getElementById("result_16").innerHTML = 'Шестнадцатричная система счисления';
		document.getElementById("res_16").innerHTML = parseInt(input, 8).toString(16);
	}
	else{
		alert("Введенные данные не являются восьмиричным кодом!");
	}
}
function translate_dec(){
	document.getElementById("result").innerHTML = '<p >Результат</p>';

	var i=document.getElementById("input");
	var input =  i.value;
	var notDec = /[^0-9a-fA-F]/;
		
	if (!notDec.test(input)){
		document.getElementById("result_2").innerHTML = 'Двоичная система счисления';
		document.getElementById("res_2").innerHTML = parseInt(input, 16).toString(2);
		document.getElementById("result_8").innerHTML = 'Восьмиричная система счисления';
		document.getElementById("res_8").innerHTML = parseInt(input, 16).toString(8);
		document.getElementById("result_10").innerHTML = 'Десятичная система счисления';
		document.getElementById("res_10").innerHTML = parseInt(input, 16).toString(10);
}
	else{
		alert("Введенные данные не являются шестнадцатиричным кодом!");
	}
}